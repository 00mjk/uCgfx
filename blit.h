#ifndef __UG_BLIT_H__
#define __UG_BLIT_H__

#include "surface.h"
#include <stdint.h>

#define UG_FLAG_COLORKEY 0x00000001
#define UG_FLAG_DOUBLESIZE 0x00000002

void UG_blit(UG_Surface *src_surface, UG_Surface *dst_surface, int x, int y, uint32_t flags, uint16_t mask);

#endif
