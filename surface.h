#ifndef __UG_SURFACE_H__
#define __UG_SURFACE_H__

#include "pixelformat.h"
#include <stdint.h>

typedef struct {
    int width;
    int height;
    int stride;
    UG_pixelformat pixelformat;
    uint32_t colorkey;
    void *data;
} UG_Surface;
#endif
